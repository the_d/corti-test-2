// Sets up the db 
var r = require('rethinkdb');
var config = require("../server/server-config.json");
var fakedata = require('./fakedata.js');

var connection = null; 

function createTableNoPromise() {
    createTable().then(() => {
        console.log("Table created.");
        return;
    }).catch( (err) => {
        console.log("Error while creating table: " + err);
        return;
    });
}

function createTable() {
    return new Promise((resolve, reject) => {
        r.connect({ host: config.db.host, port: config.db.port }, (err, conn) => {
            if (err) throw err;

            connection = conn;

            r.dbCreate(config.db.name).run(conn, (err, res) => {
                if (err) throw err;
                console.log(res);  
                
                r.db(config.db.name).tableCreate(config.db.table).run(conn, (err, res) => {
                    if(err) throw err;
                    console.log(res);
                    resolve(res);
                });
            });
        });
    });
}

function createPopulateTableNoPromise() {
    createAndPopulateTable().then( () => {
        console.log("Table created and populated.");
    }).catch( (err) => {
        console.log("Error while creating/populating table: " + err);
    });
}

function createAndPopulateTable() {
    return createTable().then(() => {
        r.db(config.db.name).table(config.db.table).insert(fakedata.generateData()).run(connection, (err, res) => {
            if (err) throw err;
            console.log(res);
            connection.close();
        });
    });
}

function populateTable() {
    r.connect({ host: config.db.host, port: config.db.port }, (err, conn) => {
        r.db(config.db.name).table(config.db.table).insert(fakedata.generateData()).run(conn, (err, res) => {
            if (err) throw err;
            console.log(res);
            conn.close();
        });
    });
}

function addOneEntry() {
    r.connect({ host: config.db.host, port: config.db.port }, (err, conn) => {
        r.db(config.db.name).table(config.db.table).insert(fakedata.generateData(1, 1)).run(conn, (err, res) => {
            if (err) throw err;
            console.log(res);
            conn.close();
        });
    });
}

function teardown() {
    return new Promise((resolve, reject) => {
        r.connect({ host: config.db.host, port: config.db.port }, (err, conn) => {
            if (err) throw err;

            r.dbDrop(config.db.name).run(conn, (err, res) => {
                if (err) {
                    reject(err);
                } else {
                    console.log(res);  
                    conn.close();
                    resolve(res);
                }
            });
        });
    });
}

module.exports = {createTable, createTableNoPromise, populateTable, createAndPopulateTable, createPopulateTableNoPromise, teardown, addOneEntry};
