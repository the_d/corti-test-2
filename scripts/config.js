var testconfig = {}; 

testconfig.deviceCount = 10;
testconfig.totalEntries = 141;
testconfig.startDate = new Date(2017, 05, 26).getTime();
testconfig.predictionMaxTime = 10;

module.exports = testconfig;