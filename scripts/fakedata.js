const randomInt = require('random-int');
const cryptoRandomString = require('crypto-random-string');
const config = require('./config');


/** generates the given number of devices, each with unique random ids */
function generateRandomDevices(devices) {
    var result = [];
    var i;
    for (i = 0; i < devices; i++) {
        result.push(cryptoRandomString(10));
    }

    return result;
}

/** 
 * Generates random DB entries, based on the schema. 
 * @param deviceCount - number of unique devices to generate
 * @param totalEntries - the total number of entries in the DB
 * @param startDate - unix timestamp of the first date since when to generate the entries.
 * @param predictionMaxTime - max number of seconds the prediction took. Default value is 10 seconds
 * @param onlyCAs - all calls will be CAs
 */
function generateData(deviceCount = config.deviceCount, 
    totalEntries = config.totalEntries, 
    startDate = config.startDate, 
    predictionMaxTime = config.predictionMaxTime,
    onlyCAs = false) {

    console.log(`Will generate data for devices=${deviceCount}, totalEntries=${totalEntries}, 
        startData=${startDate}, predictionMaxTime=${predictionMaxTime}`);    

    // generate devices;
    var devices = generateRandomDevices(deviceCount);
    
    // compute the number milliseconds since the startDate.
    // this will be used to generate random dates.
    var millisecondsSinceStartDate = Date.now() - startDate;

    var entries = [];
    var i; 
    for (i = 0; i < totalEntries; i++) {
        var entry = {
            deviceId: devices[randomInt(deviceCount-1)],
            createdAt: startDate + randomInt(millisecondsSinceStartDate),
            prediction: {
                predictionTime: randomInt(predictionMaxTime),
                ca: !onlyCAs && randomInt(0, 1) == 0 ? false : true
            }
        };

        entries.push(entry);
    }

    console.log(entries);
    return entries;
}

module.exports = {generateData};