export type IConfig = {
    serverport: number;
    db: {
        host: string;
        port: number;
        name: string;
        table: string;
    }
}