import * as express from 'express';
import * as http from "http";
import * as io from "socket.io";
import * as path from "path";
import * as log from "loglevel";

import { DBHandler, DBEntry } from "./calls" ;
import { Stats, StatsData } from "./stats";

// import the server config
import { IConfig } from "./server-config";
var config = require("../server/server-config.json") as IConfig;


export class Server {
    /** Our Express app */
    public readonly app : express.Application;

    public readonly httpServer; 
    public readonly socketIo; 

    private static singleton = new Server();

    private dbhandler: DBHandler;
    private stats: Stats;

    private activeSockets: Map<string, SocketIO.Socket> = new Map<string, SocketIO.Socket>();

    private constructor() {
        log.setLevel("debug");

        //create expressjs application
        this.app = express();

        this.httpServer = new http.Server(this.app);
        this.socketIo = io(this.httpServer);

        //configure application
        this.configure();
        
        //add routes
        this.routes();    
        
        //start additional services
        this.init();
    }

    public static instance() : Server {
        return Server.singleton;
    }

    private configure() {
        this.app.set('port', config.serverport);
        
        // tell Express to serve files from our client folder
        this.app.use(express.static(path.join(__dirname, '../client')));
       
        // configure the http server to log out
        this.httpServer.listen(this.app.get('port'), () => {
            log.info(`listening on *: ${this.app.get('port')}`);
        });
    }

    private routes() {
        let router = express.Router();

        router.get('/', (req: express.Request, res: express.Response, next: express.NextFunction) => {
            res.sendFile(path.join(__dirname, '../client/dist/index.html'));
        });

        this.app.use(router);
    }

    private init() {
        this.dbhandler = new DBHandler(config);
        this.stats = new Stats(this.dbhandler);
        
        this.dbhandler.connect().then(
            () => { 
                this.stats.assembleStats().then( () => {
                    log.debug("Stats assembled. Ready to send out. ");
                }).catch( (err) => {
                    log.error(`An error occurred while assembling stats:  ${err}`);
                    // TODO (dota) -- seriously problems happened here. 
                    // restart? 
                });

                console.info(`Connected to DB.`); 
                this.dbhandler.registerChangeListener((err, cursor) => {
                    // the DB was updates, get stats and send them out. 
                
                    if (err) {
                        log.error(`An error occurred while being updated on DB changes: ${err}`);
                        // TODO (dota) error handling
                        return;
                    }
                    
                    cursor.each((err, row) => {      
                        log.debug(`DB Updated. Row value: ${JSON.stringify(row)}`);

                        if (row.new_val != null && row.old_val == null) {
                            // insert in the DB
                            this.stats.addEntry(row.new_val).then((response: StatsData) => {
                                log.debug(`Stats updated after entry: ${JSON.stringify(response)}`);
                                this.notifyClients(response);
                            }).catch (err => {
                                log.error(`An error occurred while handling new entry: ${err}`);
                            });
                        } else if (row.new_val == null && row.old_val != null) {
                            // remove from the DB
                            // TODO (dota) ignore this for now.
                        } else {
                            // value update
                            // TODO (dota) ignore this for now.
                        }
                });
            });
        }, (reason) => {
            log.error(`An error occurred while connecting to DB: ${reason}`);
            throw reason;
        });

        this.socketIo.on('connection', socket => {
            this.activeSockets.set(socket.id, socket);     

            // a user has started the dashboard
            log.info(`New connection from ${socket.id}: ${socket.conn.remoteAddress}. We now have ${this.activeSockets.size} clients connected.`);
            log.info(`Notifying ${socket.id} of stats.`);
            if (this.dbhandler.isConnected()) {
                socket.emit('updated-stats', this.stats.getStatsData());
            } else {
                log.error(`We are not connected to the DB. `);
            }
            
            socket.on('disconnect', () => {
                // a user has left the page
                this.activeSockets.delete(socket.id);
                log.info(`Connection from ${socket.id} disconnected.`);
            });
        });
    }

    private notifyClients(stats: StatsData) {
        if (this.activeSockets.size == 0) {
            return;
        }

        log.info(`Notifying ${this.activeSockets.size} of updated stats.`);
        this.socketIo.sockets.emit('updated-stats', stats);
    }
}



