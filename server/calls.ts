var rethinkdb = require('rethinkdb');
import { IConfig } from "./server-config";

import * as log from "loglevel";

export type DBEntry = {
    // unique id of the entry
    id: number,
    // unique id of the device
    deviceId: string,
    // unix timestamp
    createdAt: number,
    prediction: {
        // time in seconds
        predictionTime: number,
        // true if a CA was predicted.
        ca: boolean
    }
}

export class DBHandler {
    private connection;
    constructor(private readonly config: IConfig) {
        log.debug(`DHHandler initialized with ${JSON.stringify(config)}`);
    }

    /** 
     * Connects to the DB and returns a promise with the result. 
     */
    public connect(): Promise<void> {
        if (this.isConnected()) {
            // if we already have a live connection -- pass that one.
            return Promise.resolve();
        }

        let connectOptions /* : rethinkdb.ConnectionOptions */ = {
            host: this.config.db.host,
            port: this.config.db.port
        };

        return new Promise<void>((resolve, reject) => {
            rethinkdb.connect(connectOptions, (err, conn) => {
                if (err) {
                    log.error(`An error occurred while connecting to DB. Details ${err.message}.`);
                    reject(err);
                } else {
                    this.connection = conn;
                    resolve();
                }
            });
        });
    }

    public disconnect(): Promise<void> {
        if (this.isConnected()) {
            return this.connection.close();
        } else {
            this.connection === undefined;
            return Promise.resolve();
        }
    }

    private getTable() {
        return rethinkdb.db(this.config.db.name)
        .table(this.config.db.table);
    }

    /** 
     * Returns a promise that will resolve to an array of {group, reduction},
     * where:
     * * group is the day (in epoch time)
     * * reduction is the number of actual CA calls on that particular day. 
     * 
     * If a day did not have any calls, or any calls with CA calls, that day will 
     * be missing from the result.
     * 
     * If any errors are occured, the promise will fail.
     * 
     */
    public getCAHistory() {
        if (!this.isConnected()) {
            return Promise.reject(`Not connected to db`);
        }

        return this.caHistoryQuery().run(this.connection);
    }

    /**
     * The actual query to get the CA history.
     */
    private caHistoryQuery() {
        return this.getTable()
        .filter({
            'prediction': {
                'ca': true
            }
        })
        .pluck('createdAt',
            { 
                'prediction': ['ca']
            }
        )
        .orderBy(rethinkdb.asc('createdAt'))
        .group(rethinkdb.epochTime(
            rethinkdb.row('createdAt')
            // divide to get per days. 
            .div(24*60*60*1000).floor().mul(24*60*60))
            .date().toEpochTime()
            // and get back with milliseconds
            .mul(1000))
        .count(); 
    }

    /** 
     * Returns a promise that will resolve to the number of unique deployed devices. 
     */
    public getDeployedDevicesCount() {
        if (!this.isConnected()) {
            return Promise.reject(`Not connected to db`);
        }

        return this.getTable()
            .withFields('deviceId')
            .distinct()
            .count()
            .run(this.connection);
    }

    /** 
     * Returns a promise that will resolve to the date of the first call. 
     */
    public getDateOfFirstCall() {
        if (!this.isConnected()) {
            return Promise.reject(`Not connected to db`);
        }

        return this.getTable()
            .withFields('createdAt')
            .distinct()
            .orderBy(rethinkdb.asc('createdAt'))
            .nth(0)
            .getField('createdAt')
            .run(this.connection);
    }

    /** 
     * Returns a promise that will resolve to the total number of calls. 
     */
    public getTotalNumberOfCalls() {
        if (!this.isConnected()) {
            return Promise.reject(`Not connected to db`);
        }

        return this.getTable()
            .count()
            .run(this.connection);
    }

    /** 
     * Returns a promise that will resolve to the total number of CA calls. 
     */
    public getTotalNumberOfCACalls() {
        if (!this.isConnected()) {
            return Promise.reject(`Not connected to db`);
        }

        return this.getTable()
            .filter({
                'prediction': {
                    'ca': true
                }
            })
            .count()
            .run(this.connection);
    }

    /** 
     * Returns a promise that will resolve to the average time to detection. 
     * Will fail if there are no calls.
     * 
     */
    public getAvgTimeToDetection() {
        if (!this.isConnected()) {
            return Promise.reject(`Not connected to db`);
        }

        return this.getTable()
            .filter({
                'prediction': {
                    'ca': true
                }
            }) //.branch(rethinkdb.isEmpty().not()
            .avg(function (call) {
                return call('prediction')('predictionTime');
            })
            .default(0)
            .run(this.connection);
    }

    /** 
     * Returns a promise that will resolve if the listener was registered successfully. 
     * The listener is a function to be called back whenever there are changes to the DB.
     * 
     * The listener has the following signature: function(err, cursor). 
     * See https://www.rethinkdb.com/docs/changefeeds/javascript/ for more details
     * 
     */
    public registerChangeListener(listener) {
        if (!this.isConnected()) {
            return Promise.reject(`Not connected to db`);
        }

        return this.getTable()
            .changes({squash: true})
            // .filter(rethinkdb.row('old_val').eq(null))
            .run(this.connection, listener);
    }

    public isConnected() {
        return this.connection !== undefined && this.connection.open;
    }

    // ugly way to expose the connection for testing.
    public getConnection() {
        return this.connection;
    }
}