import {DBHandler, DBEntry} from "./calls";
import * as log from "loglevel";


/**
 * Cardiac arrest history, as an array of {number, number}, where:
 * * group is the day, in epoch time.
 * * reduction is the number of CA calls on that day.
 */
export type CAHistory = { group: number, reduction: number }[];

export type StatsData = {
    chartData: CAHistory,
    devicesDeployed: number,
    daysSinceStart: number,
    callsSinceStart: number,
    caCallsSinceStart: number,
    avgTimeToDetection: number,
}

export class Stats {
    public static readonly MS = 1000;
    public static readonly DAY_IN_SECONDS = 24 * 60 * 60;
    public static readonly DAY_IN_MS = Stats.DAY_IN_SECONDS * Stats.MS; 

    private _chartData: CAHistory;
    private _devicesDeployed = 0;
    private _firstDate = 0;
    private _daysSinceStart = 0;
    private _callsSinceStart = 0;
    private _caCallsSinceStart = 0;
    private _avgTimeToDetection = 0;
    private _totalTimeToDetection = 0;
    private _noOfDetections = 0;

    constructor(private readonly dbhandler: DBHandler) {
    }

    public getStatsData(): StatsData {
        return {
            chartData: this._chartData,
            devicesDeployed: this._devicesDeployed,
            daysSinceStart: this._daysSinceStart,
            callsSinceStart: this._callsSinceStart,
            caCallsSinceStart: this._caCallsSinceStart,
            avgTimeToDetection: this._avgTimeToDetection
        }
    }

    /** 
     * Computes the number of days between the two dates. 
     * The dates should provided as milliseconds since the epoch. 
     */
    public static daysBetween(date1, date2) {
       return Math.round(Math.abs(date1 - date2) / Stats.DAY_IN_MS); 
    }

    public addEntry(entry: DBEntry): Promise<StatsData> {
        if (!this.dbhandler.isConnected()) {
            log.warn(`Unable to get stats -- not connected to the DB`);
            return Promise.reject("Not connected to DB.");
        }

        if (entry === undefined) {
            log.error("Unable to process entry -- undefined. ");
            return Promise.reject("Undefined entry.");
        }

        this._daysSinceStart = Stats.daysBetween(Date.now(), this._firstDate);
        this._callsSinceStart ++; 
        if (entry.prediction.ca) {
            this._caCallsSinceStart ++;
        }

        // Truncate the date to get ignore HH:MM:SS. 
        let createdAt = Math.floor(entry.createdAt / Stats.DAY_IN_MS) * Stats.DAY_IN_SECONDS * Stats.MS;
        
        // This is already ordered. 
        for (let i = this._chartData.length - 1; i>=0; i--){
            if (this._chartData[i].group === createdAt) {
                this._chartData[i].reduction ++; 
                break;
            } else if (this._chartData[i].group < createdAt) {
                // This is a first for this date -- add it to the CAHistory
                this._chartData.splice(i, 0, {group: createdAt, reduction: 1});
                break;
            }
        }

        return this.dbhandler.getDeployedDevicesCount().then(result => {
            log.debug(`Got devicesDeployed result:  ${result}`);
            // finalStats.
            this._devicesDeployed = result;
    
            return this.dbhandler.getAvgTimeToDetection();
        }).then(result => {
            log.debug(`Got avgTimeToDetection result.  ${result}`);
            // finalStats.
            this._avgTimeToDetection = result;

            return Promise.resolve(this.getStatsData());
        });
    }

    /**
     * Assembles stats.
     * 
     * This method must be called only after the DBHandler passed to the constructor has connected
     * to the DB. 
     * 
     * Calling this method will do a full inquiry of the DB and rewrite the existing stats.
     * 
     * @return a Promise that resolves if the data structure is ready to be sent to clients. 
     */
    public assembleStats(): Promise<void> {
        var finalStats = {};
        var chartData = {};
        var devicesDeployed = 0;
        var firstDate = 0;
        var daysSinceStart = 0;
        var callsSinceStart = 0;
        var caCallsSinceStart = 0;
        var avgTimeToDetection = 0;

        if (!this.dbhandler.isConnected()) {
            log.warn("assembleStats(): Unable to get stats -- not connected to the DB");
            return Promise.reject("Not connected to DB.");
        }

        log.debug('will compute stats');

        // Chaining promises to get the result. 
        // There must be an easier way than this ... 
        return this.dbhandler.getCAHistory().then(result => {
            log.debug(`Got CA History result: ${JSON.stringify(result)}`);
            // finalStats.
            chartData = result;

            return this.dbhandler.getDeployedDevicesCount();
        }).then(result => {
            log.debug(`Got devicesDeployed result: ${JSON.stringify(result)}`);
            // finalStats.
            devicesDeployed = result;

            return this.dbhandler.getDateOfFirstCall();
        }).then(result => {
            log.debug(`Got daysSinceStart result: ${JSON.stringify(result)}`);
            // finalStats.
            firstDate = result;
            daysSinceStart = Stats.daysBetween(Date.now(), result);

            return this.dbhandler.getTotalNumberOfCalls();
        }).then(result => {
            log.debug(`Got callsSinceStart result: ${JSON.stringify(result)}`);
            // finalStats.
            callsSinceStart = result;

            return this.dbhandler.getTotalNumberOfCACalls();
        }).then(result => {
            log.debug(`Got caCallsSinceStart result: ${JSON.stringify(result)}`);
            // finalStats.
            caCallsSinceStart = result;

            return this.dbhandler.getAvgTimeToDetection();
        }).then(result => {
            log.debug(`Got avgTimeToDetection result: ${JSON.stringify(result)}`);
            // finalStats.
            avgTimeToDetection = result;

            this._chartData = <CAHistory> chartData;
            this._devicesDeployed = devicesDeployed;
            this._daysSinceStart = daysSinceStart;
            this._firstDate = firstDate;
            this._callsSinceStart = callsSinceStart;
            this._caCallsSinceStart = caCallsSinceStart;
            this._avgTimeToDetection = avgTimeToDetection;

            return Promise.resolve();
        });
    }
}