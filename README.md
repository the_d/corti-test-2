# Calls Dashboard

Implementation for Task nr. 2, based on node.js, express, rethinkdb, and vue (nerv?).

Prerequisites:

* node.js
* express
* rethinkdb

Structure:
* build/ -- the .js files resulted from building the server code. 
* client/ -- the folder containing client-side code
* scripts/ -- js scripts to setup the DB.
* server/ -- the server-side code.
* tests/ -- folder containing test files.

Steps: 

* Install the prerequisites.
* Run npm install to install the components. 
* Server-side:
** Start rethinkdb. 
** Setup the database by running `npm run setupdb`. 
** Compile the server by running `tsc` in the project folder.
** Run the server-side operation by running `node built/app.js`. 
** Populate the DB with a bulk of fake data using `npm run fake.db.populate` and see how the bar of the dashboard updates itself. Or run `npm run fake.db.addentry` to add one entry to the db.
** Run tests via `npm run test`, or `npm run test:coverage` to also have code coverage tests.
* Client-side:
** Run `npm run build.client` to compile the client-side code. The compiled files will be set in client/dist. 
** Access the dashboard at `www.localhost:9000`

Limitations:

* Since an async process is pushing data into the DB, the server doesn't validate the input. 

Notes: some of the scripts in `./scripts/` are not super robust, e.g., they will throw an error if you try to populate an inexistent database. 

