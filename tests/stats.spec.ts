import 'mocha';
import {expect, assert} from 'chai';
import {DBHandler} from '../server/calls';
import {Stats, StatsData} from "../server/stats";
import {IConfig} from "../server/server-config";
import * as log from "loglevel";

var r = require('rethinkdb');
var setup = require('../scripts/setup.js');
var config = require('../server/server-config.json') as IConfig;

log.setLevel("info");

before( (done) => {
    setup.teardown().then(() => {
        done();
    }).catch(() => {
        done();
    });
});

function createEntry(deviceId, createdAt, ca, predictionTime = 9) {
    return {
        deviceId: deviceId,
        createdAt: createdAt,
        prediction: {
            predictionTime: predictionTime,
            ca: ca
        }
    };
}

function createEntryWithID(id, deviceId, createdAt, ca, predictionTime = 9) {
    return {
        id: id,
        deviceId: deviceId,
        createdAt: createdAt,
        prediction: {
            predictionTime: predictionTime,
            ca: ca
        }
    };
}



describe('stats', () => {
    let firstDayInMs = new Date(Date.UTC(2017, 6, 1)).getTime();
    let firstDayInMsPlus = new Date(Date.UTC(2017, 6, 1, 12, 30)).getTime();
    let secondDayInMs = new Date(Date.UTC(2017, 6, 2)).getTime();
    let lastDayInMs = new Date(Date.UTC(2017, 10, 2)).getTime();

    let dbhandler: DBHandler;
    let stats: Stats;

    beforeEach(() => {
        dbhandler = new DBHandler(config);
        stats = new Stats(dbhandler);
        return setup.createTable();
    });
    
    afterEach(() => {
        return dbhandler.disconnect().then(() => {
            return setup.teardown();
        });
    });

    describe('assemble stats', () => {
        it('should assemble stats for a single entry', done => {    
            let entries = [createEntry("a", firstDayInMs, true, 1)];

            dbhandler.connect().then(() => {
                r.db(config.db.name).table(config.db.table).insert(entries).run(dbhandler.getConnection(), (err, res) => {
                    if (err) throw err;
                    stats.assembleStats().then(() => { 
                        let result = stats.getStatsData();
                        assert.equal(result.avgTimeToDetection, 1, "Avg time to detection.");
                        assert.equal(result.caCallsSinceStart, 1, "CA Calls since stats.");
                        assert.equal(result.callsSinceStart, 1, "Calls since start.");
                        assert.equal(result.daysSinceStart, Stats.daysBetween(firstDayInMs, Date.now()), "Days since start.");
                        assert.equal(result.devicesDeployed, 1, "Devices deployed.");
                        assert.equal(result.chartData.length, 1, "Chart data");
                        done();
                    }, (reason) => {
                        done(reason);
                    }).catch((reason) => {
                        console.log(reason);
                        done(reason);
                    });        
                });
            }, reason => {
                done(reason);
            });
        });

        it('should assemble stats for two entries, same day', done => {    
            let entries = [createEntry("a", firstDayInMs, true, 1), createEntry("b", firstDayInMsPlus, true, 1)];

            dbhandler.connect().then(() => {
                r.db(config.db.name).table(config.db.table).insert(entries).run(dbhandler.getConnection(), (err, res) => {
                    if (err) throw err;
                    stats.assembleStats().then(() => { 
                        let result = stats.getStatsData();
                        assert.equal(result.avgTimeToDetection, 1, "Avg time to detection.");
                        assert.equal(result.caCallsSinceStart, 2, "CA Calls since stats.");
                        assert.equal(result.callsSinceStart, 2, "Calls since start.");
                        assert.equal(result.daysSinceStart, Stats.daysBetween(firstDayInMs, Date.now()), "Days since start.");
                        assert.equal(result.devicesDeployed, 2, "Devices deployed.");
                        assert.equal(result.chartData.length, 1, "Chart data");
                        done();
                    }, (reason) => {
                        done(reason);
                    }).catch((reason) => {
                        console.log(reason);
                        done(reason);
                    });        
                });
            }, reason => {
                done(reason);
            });
        });

        it('should assemble stats for two entries, two day', done => {    
            let entries = [createEntry("a", firstDayInMs, true, 1), createEntry("b", secondDayInMs, true, 3)];

            dbhandler.connect().then(() => {
                r.db(config.db.name).table(config.db.table).insert(entries).run(dbhandler.getConnection(), (err, res) => {
                    if (err) throw err;
                    stats.assembleStats().then(() => { 
                        let result = stats.getStatsData();
                        assert.equal(result.avgTimeToDetection, 2, "Avg time to detection.");
                        assert.equal(result.caCallsSinceStart, 2, "CA Calls since stats.");
                        assert.equal(result.callsSinceStart, 2, "Calls since start.");
                        assert.equal(result.daysSinceStart, Stats.daysBetween(firstDayInMs, Date.now()), "Days since start.");
                        assert.equal(result.devicesDeployed, 2, "Devices deployed.");
                        assert.equal(result.chartData.length, 2, "Chart data");
                        done();
                    }, (reason) => {
                        done(reason);
                    }).catch((reason) => {
                        console.log(reason);
                        done(reason);
                    });        
                });
            }, reason => {
                done(reason);
            });
        });
    });

    describe('add entry', () => {
        it('add entry: should assemble stats for two entries, same day', done => {    
            let entries = [createEntry("a", firstDayInMs, true, 1)];

            dbhandler.connect().then(() => {
                r.db(config.db.name).table(config.db.table).insert(entries).run(dbhandler.getConnection(), (err, res) => {
                    if (err) throw err;
                    stats.assembleStats().then(() => { 
                        let result = stats.getStatsData();

                        log.debug(`First stats: ${JSON.stringify(result)}`);
                        assert.equal(result.caCallsSinceStart, 1, "CA Calls since stats.");
                        assert.equal(result.callsSinceStart, 1, "Calls since start.");
                        assert.equal(result.daysSinceStart, Stats.daysBetween(firstDayInMs, Date.now()), "Days since start.");
                        assert.equal(result.chartData.length, 1, "Chart data");
                    }).then(() => {
                        let entry = createEntryWithID(1, "b", firstDayInMsPlus, true, 1);
                        
                        stats.addEntry(entry).then( (statsData: StatsData) => {
                            log.debug(`Stats after new entry: ${JSON.stringify(statsData)}`);
                            assert.equal(statsData.caCallsSinceStart, 2, "CA Calls since stats.");
                            assert.equal(statsData.callsSinceStart, 2, "Calls since start.");
                            assert.equal(statsData.daysSinceStart, Stats.daysBetween(firstDayInMs, Date.now()), "Days since start.");
                            assert.equal(statsData.chartData.length, 1, "Chart data");
                            done();
                        });
                    });    
                });
            }).catch((err) => {
                done(err);
            });
        });

        it('add entry: should assemble stats for two entries, two day', done => {    
            let entries = [createEntry("a", firstDayInMs, true, 1)];

            dbhandler.connect().then(() => {
                r.db(config.db.name).table(config.db.table).insert(entries).run(dbhandler.getConnection(), (err, res) => {
                    if (err) throw err;
                    stats.assembleStats().then(() => { 
                        let result = stats.getStatsData();
                       
                        assert.equal(result.caCallsSinceStart, 1, "CA Calls since stats.");
                        assert.equal(result.callsSinceStart, 1, "Calls since start.");
                        assert.equal(result.daysSinceStart, Stats.daysBetween(firstDayInMs, Date.now()), "Days since start.");
                        assert.equal(result.chartData.length, 1, "Chart data");    
                    }). then( () => {
                        let entry = createEntryWithID(1, "b", secondDayInMs, true, 1);
                        
                        stats.addEntry(entry).then( (statsData: StatsData) => {
                            assert.equal(statsData.caCallsSinceStart, 2, "CA Calls since stats.");
                            assert.equal(statsData.callsSinceStart, 2, "Calls since start.");
                            assert.equal(statsData.daysSinceStart, Stats.daysBetween(firstDayInMs, Date.now()), "Days since start.");
                            assert.equal(statsData.chartData.length, 2, "Chart data");
                            done();
                        });
                    }, (reason) => {
                        done(reason);
                    }).catch((reason) => {
                        console.log(reason);
                        done(reason);
                    });        
                });
            }, reason => {
                done(reason);
            });
        });
    });
});