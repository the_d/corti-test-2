import 'mocha';
import { expect, assert } from 'chai';
import { DBHandler } from '../server/calls';
import { IConfig } from "../server/server-config";

var r = require('rethinkdb');
var setup = require('../scripts/setup.js');
var config = require('../server/server-config.json') as IConfig;

before((done) => {
    setup.teardown().then(() => {
        done();
    }).catch(() => {
        done();
    });
});



function createEntry(deviceId, createdAt, ca, predictionTime = 9) {
    return {
        deviceId: deviceId,
        createdAt: createdAt,
        prediction: {
            predictionTime: predictionTime,
            ca: ca
        }
    };
}

describe('calls', () => {
    let firstDayInMs = new Date(Date.UTC(2017, 6, 1)).getTime();
    let firstDayInMsPlus = new Date(Date.UTC(2017, 6, 1, 12, 30)).getTime();
    let secondDayInMs = new Date(Date.UTC(2017, 6, 2)).getTime();
    let lastDayInMs = new Date(Date.UTC(2017, 10, 2)).getTime();

    let dbhandler: DBHandler;

    beforeEach(() => {
        dbhandler = new DBHandler(config);
        return setup.createTable();
    });

    afterEach(() => {
        return dbhandler.disconnect().then(() => {
            return setup.teardown();
        });
    });

    describe('getCAHistory()', () => {
        it('should return only 1 entry in DB with only one actual CA', done => {
            let entries = [createEntry("a", firstDayInMs, true), createEntry("b", secondDayInMs, false)];

            dbhandler.connect().then(() => {
                r.db(config.db.name).table(config.db.table).insert(entries).run(dbhandler.getConnection(), (err, res) => {
                    if (err) throw err;
                    dbhandler.getCAHistory().then((result) => {
                        console.log(result);
                        assert.equal(result.length, 1, "More then one grouping returned");
                        assert.equal(result[0]["group"], firstDayInMs, "Time");
                        assert.equal(result[0]["reduction"], 1, "count");
                        done();
                    }, (reason) => {
                        done(reason);
                    }).catch((reason) => {
                        console.log(reason);
                        done(reason);
                    });
                });
            }, reason => {
                done(reason);
            });
        });

        it('should return only 1 entry with 2 reduction in DB with two CAs on same day', (done) => {
            let entries = [createEntry("a", firstDayInMs, true),
            createEntry("b", secondDayInMs, false),
            createEntry("c", firstDayInMsPlus, true)];

            dbhandler.connect().then(() => {
                r.db(config.db.name).table(config.db.table).insert(entries).run(dbhandler.getConnection(), (err, res) => {
                    if (err) throw err;
                    dbhandler.getCAHistory().then((result) => {
                        assert.equal(result.length, 1, "More then one grouping returned");
                        assert.equal(result[0].group, firstDayInMs, "Time");
                        assert.equal(result[0].reduction, 2, "count");
                        done();
                    }, (reason) => {
                        done(reason);
                    }).catch((reason) => {
                        console.log(reason);
                        done(reason);
                    });
                });
            }, (reason) => {
                done(reason);
            });
        });

        it('should return only 2 ordered dates with 1 reduction each', (done) => {
            let entries = [createEntry("a", secondDayInMs, true),
            createEntry("b", firstDayInMs, false),
            createEntry("c", firstDayInMs, true)];

            dbhandler.connect().then(() => {
                r.db(config.db.name).table(config.db.table).insert(entries).run(dbhandler.getConnection(), (err, res) => {
                    if (err) throw err;
                    dbhandler.getCAHistory().then((result) => {
                        console.log(result);
                        assert.equal(result.length, 2, "More then one grouping returned");
                        assert.equal(result[0].group, firstDayInMs, "Time");
                        assert.equal(result[0].reduction, 1, "count");
                        assert.equal(result[1].group, secondDayInMs, "Time");
                        assert.equal(result[1].reduction, 1, "count");
                        done();
                    }, (reason) => {
                        done(reason);
                    }).catch((reason) => {
                        console.log(reason);
                        done(reason);
                    });
                });
            });
        });
    });

    describe('getDeployedDevicesCount()', () => {
        it('should return only 1 entry ', (done) => {
            let entries = [createEntry("a", firstDayInMs, true), createEntry("a", secondDayInMs, false)];

            dbhandler.connect().then(() => {
                r.db(config.db.name).table(config.db.table).insert(entries).run(dbhandler.getConnection(), (err, res) => {
                    if (err) throw err;
                    dbhandler.getDeployedDevicesCount().then((result) => {
                        console.log(result);
                        assert.equal(result, 1, "More then one grouping returned");
                        done();
                    }, (reason) => {
                        done(reason);
                    }).catch((reason) => {
                        console.log(reason);
                        done(reason);
                    });
                });
            }, function (reason) {
                done(reason);
            });
        });

        it('should return only 2 entries ', (done) => {
            let entries = [createEntry("a", firstDayInMs, true), createEntry("b", secondDayInMs, false)];

            dbhandler.connect().then(() => {
                r.db(config.db.name).table(config.db.table).insert(entries).run(dbhandler.getConnection(), (err, res) => {
                    if (err) throw err;
                    dbhandler.getDeployedDevicesCount().then((result) => {
                        console.log(result);
                        assert.equal(result, 2);
                        done();
                    }, (reason) => {
                        done(reason);
                    }).catch((reason) => {
                        console.log(reason);
                        done(reason);
                    });
                });
            }, function (reason) {
                done(reason);
            });
        });

        it('should return 0 entries ', (done) => {
            let entries = [];

            dbhandler.connect().then(() => {
                r.db(config.db.name).table(config.db.table).insert(entries).run(dbhandler.getConnection(), (err, res) => {
                    if (err) throw err;
                    dbhandler.getDeployedDevicesCount().then((result) => {
                        console.log(result);
                        assert.equal(result, 0);
                        done();
                    }, (reason) => {
                        done(reason);
                    }).catch((reason) => {
                        console.log(reason);
                        done(reason);
                    });
                });
            }, function (reason) {
                done(reason);
            });
        });
    });

    describe('getDateFirstCall()', () => {
        it('simpleTest', (done) => {
            let entries = [
                createEntry("a", lastDayInMs, true),
                createEntry("b", secondDayInMs, false),
                createEntry("c", firstDayInMs, true),
                createEntry("d", firstDayInMsPlus, false),
            ];

            dbhandler.connect().then(() => {
                r.db(config.db.name).table(config.db.table).insert(entries).run(dbhandler.getConnection(), (err, res) => {
                    if (err) throw err;
                    dbhandler.getDateOfFirstCall().then((result) => {
                        console.log(result);
                        assert.equal(result, firstDayInMs);
                        done();
                    }, (reason) => {
                        done(reason);
                    }).catch((reason) => {
                        console.log(reason);
                        done(reason);
                    });
                });
            }, function (reason) {
                done(reason);
            });
        });
    });

    describe('getTotalNumberOfCalls()', () => {
        it('simpleTest', (done) => {
            let entries = [
                createEntry("a", lastDayInMs, true),
                createEntry("b", secondDayInMs, false),
                createEntry("c", firstDayInMs, true),
                createEntry("d", firstDayInMsPlus, false),
            ];

            dbhandler.connect().then(() => {
                r.db(config.db.name).table(config.db.table).insert(entries).run(dbhandler.getConnection(), (err, res) => {
                    if (err) throw err;
                    dbhandler.getTotalNumberOfCalls().then((result) => {
                        console.log(result);
                        assert.equal(result, 4);
                        done();
                    }, (reason) => {
                        done(reason);
                    }).catch((reason) => {
                        console.log(reason);
                        done(reason);
                    });
                });
            }, function (reason) {
                done(reason);
            });
        });
    });

    describe('getTotalNumberOfCACalls()', () => {
        it('simpleTest', (done) => {
            let entries = [
                createEntry("a", lastDayInMs, true),
                createEntry("b", secondDayInMs, false),
                createEntry("c", firstDayInMs, true),
                createEntry("d", firstDayInMsPlus, false),
            ];

            dbhandler.connect().then(() => {
                r.db(config.db.name).table(config.db.table).insert(entries).run(dbhandler.getConnection(), (err, res) => {
                    if (err) throw err;
                    dbhandler.getTotalNumberOfCACalls().then((result) => {
                        console.log(result);
                        assert.equal(result, 2);
                        done();
                    }, (reason) => {
                        done(reason);
                    }).catch((reason) => {
                        console.log(reason);
                        done(reason);
                    });
                });
            }, function (reason) {
                done(reason);
            });
        });
    });

    describe('getAvgTimeToDetection()', () => {
        it('simpleTest', (done) => {
            let entries = [
                createEntry("a", lastDayInMs, true, 4),
                createEntry("b", secondDayInMs, false),
                createEntry("c", firstDayInMs, true, 6),
                createEntry("d", firstDayInMsPlus, false),
            ];

            dbhandler.connect().then(() => {
                r.db(config.db.name).table(config.db.table).insert(entries).run(dbhandler.getConnection(), (err, res) => {
                    if (err) throw err;
                    dbhandler.getAvgTimeToDetection().then((result) => {
                        console.log(result);
                        assert.equal(result, 5);
                        done();
                    }, (reason) => {
                        done(reason);
                    }).catch((reason) => {
                        console.log(reason);
                        done(reason);
                    });
                });
            }, function (reason) {
                done(reason);
            });
        });

        it('no calls', (done) => {
            let entries = [];

            dbhandler.connect().then(() => {
                r.db(config.db.name).table(config.db.table).insert(entries).run(dbhandler.getConnection(), (err, res) => {
                    if (err) throw err;
                    dbhandler.getAvgTimeToDetection().then((result) => {
                        console.log(result);
                        assert.equal(result, 0);
                        done();
                    }, (reason) => {
                        done(reason);
                    }).catch((reason) => {
                        console.log(reason);
                        done(reason);
                    });
                });
            }, function (reason) {
                done(reason);
            });
        });
    });

    // describe('registerChangeListener', () => {
    //     it('should notify of changes', (done) => {
    //         let entries = [
    //             createEntry("a", lastDayInMs, true, 4),
    //         ];

    //         dbhandler.connect().then(() => {
    //             dbhandler.registerChangeListener((err, cursor) => {
    //                 if (err) {
    //                     done(err);
    //                 } else {
    //                     done();
    //                 }
    //             });
    //         }).then(() => {
    //             return r.db(config.db.name).table(config.db.table).insert(entries).run(dbhandler.getConnection());
    //         }).catch(err => {
    //             done(err);
    //         });
    //     });
    // });
});